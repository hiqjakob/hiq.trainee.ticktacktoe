# HiQ Trainee React Template

## Hur kommer jag igång?
* Se till att `Node` med `npm` är installerat. Finns [här](https://nodejs.org/en/).
* Se till att `webpack-dev-server` är globalt installerat via npm (`npm install -g webpack-dev-server`)

### Backend
* Starta projektet (`.sln`-filen) med Visual Studio
* För att generera typdefinitioner används http://type.litesolutions.net. Gå till projektet i Visual Studio. Under `/Scripts`, högerklicka på filen `TypeLite.Net4.tt` och välj *Run Custom Tool* (kan kräva att `Build Solution` först körs i Visual Studio).

### Frontend
* Navigera till `HiQ.Trainee.ReactTemplate`-mappen
* Kör `npm install`
* För att starta dev-server med hot reloading, kör `webpack-dev-server`. Adressen blir sedan [http://localhost:8080/](http://localhost:8080/). 


## Vid fel

> Module build failed: Could not load TypeScript.

Kör `npm link typescript`

> 'webpack-dev-server' is not recognized as an internal or external command, operable program or batch file.

Kör `npm install -g webpack-dev-server`

## Vem frågar jag?

* oscar.andersson@hiq.se
* jakob.thomasson@hiq.se
* kalle.bornemark@hiq.se