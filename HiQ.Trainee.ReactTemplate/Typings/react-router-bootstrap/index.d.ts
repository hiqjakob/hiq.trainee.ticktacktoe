import { ComponentClass } from "react";
import { NavLinkProps } from "react-router-dom";

type LinkContainer = ComponentClass<NavLinkProps>;
export const LinkContainer: LinkContainer;