﻿var webpack = require('webpack');
var path = require("path");
var proxy = 'localhost:56560';

module.exports = {
    entry: [
        // activate HMR for React
        'react-hot-loader/patch',

        // the entry point of our app
        './Features/index.tsx',
    ],
    //entry: "./Scripts/src/index.tsx",
    output: {
        filename: "./Scripts/dist/bundle.js",
    },

    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",

    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },

    module: {
        loaders: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            //{ test: /\.tsx?$/, loader: "ts-loader" }
            { test: /\.tsx?$/, loader: ['react-hot-loader/webpack', 'ts-loader'] },
            { test: /\.css$/, loader: "style-loader!css-loader" }
        ]
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        // enable HMR globally

        new webpack.NamedModulesPlugin(),
        // prints more readable module names in the browser console on HMR updates

        //new webpack.NoEmitOnErrorsPlugin(),
        // do not emit compiled assets that include errors
    ],

    //externals: {
    //    'react': 'react', // Case matters here 
    //    'react-dom': 'reactDOM', // Case matters here 
    //    'react-router': 'ReactRouter'
    //},

    devServer: {
        proxy: {
            '*': {
                target: 'http://' + proxy
            }
        },
        port: 8080,
        host: '0.0.0.0',
        hot: true,
    },
}