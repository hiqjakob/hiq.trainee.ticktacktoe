﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactRouter from "react-router";
import { Switch, Route, Redirect } from "react-router-dom";
import { Col, Nav, NavItem, Badge } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import { Inventory } from './../Inventory';


export class Menu extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
        }
    }

    render() {
        const menuStyle: React.CSSProperties = {
            paddingTop: '108px'
        }

        const navStyle: React.CSSProperties = {
            borderBottom: '2px solid hotpink',
            textTransform: 'uppercase',
            fontSize: '1.4rem'
        }

        const badgeStyle: React.CSSProperties = {
            backgroundColor: 'orange'
        }

        const imageStyle: React.CSSProperties = {
            float: 'left',
            height: '150px'
        }

        const css = `
            .route-list a {
                color: #333;
            }
            .route-list .nav-tabs>li.active>a {
                background-color: hotpink;
                color: white;
            }
        `;

        const navTextStyle: React.CSSProperties = {

        }

        return (
            <div>
                <style>
                    {css}
                </style>
                <img src="/Content/yolo.jpg" alt="Lantmännen Maskin" style={imageStyle} />
                <div className="route-list" style={menuStyle}>
                    <Nav bsStyle="tabs" activeKey="1" style={navStyle}>
                        <LinkContainer to="/ticktacktoe/nogamehistory" >
                            <NavItem eventKey="1">No Game History</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/ticktacktoe/gamehistory" >
                            <NavItem eventKey="1">With Game History</NavItem>
                        </LinkContainer>


                        <LinkContainer to="/ticktacktoe/ourgame">
                            <NavItem eventKey="2">Our Game </NavItem>
                        </LinkContainer>

                    </Nav>
                </div>
            </div>
        );
    }
}