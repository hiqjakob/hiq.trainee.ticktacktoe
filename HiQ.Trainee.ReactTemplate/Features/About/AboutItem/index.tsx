﻿import * as React from "react";

interface IAboutItemProps {
    header: string;
    text: string;
}


export class AboutItem extends React.Component<IAboutItemProps, void> {
    render() {
        return (
            <div >
                <h3>{this.props.header}</h3>
                <p>{this.props.text}</p>
            </div>
        );
    }
}




