﻿import * as React from "react";
import { AboutItem} from "./AboutItem";

export interface IAboutProps {
    items: {header:string; text:string}[];
    headerText: string;
}


export class About extends React.Component<IAboutProps, void> {
    render() {
        return (
            <div>
                <h2>{this.props.headerText}</h2>
                {this.props.items.map((item, index) => <AboutItem key={index} header={item.header} text={item.text} />)}
            </div>
        );
    }
}



