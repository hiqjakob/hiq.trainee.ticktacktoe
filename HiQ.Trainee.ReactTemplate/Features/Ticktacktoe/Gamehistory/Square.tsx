import * as React from "react";

interface Props {
  value: string;
  onClick: () => void;
}




// Functional components, React will optimize them more in the future. Also cleaning up the code
const Square = (props: Props) => (
  <button className="square" onClick={() => props.onClick()}>
    {props.value}
  </button>
);

/*
class Square extends React.Component<Props, void> {
  
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <button className="square" onClick={() => this.props.onClick()}>
        {this.props.value}
      </button>
    );
  }
}*/

export default Square; 