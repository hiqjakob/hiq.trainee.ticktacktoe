import * as React from "react";
import Square from "./Square";
import { calculateWinner } from '../rules';

interface Props {
    // squares: Array<string>
}

interface State {
    squares: Array<string>,
    xIsNext: boolean
}


class Board extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            squares: Array(9).fill(null),
            xIsNext: true
        };
    }

    renderSquare(i: number) {
        return (
            <Square
                value={this.state.squares[i]}
                onClick={() => this.handleClick(i)}
            />
        );
    }


    handleClick(i: number) {
        /* 
            Shallow copies duplicate as little as possible. A shallow copy of a collection is a copy of the collection structure, not the elements. 
            With a shallow copy, two collections now share the individual elements.
            Deep copies duplicate everything. A deep copy of a collection is two collections with all of the elements in the original collection duplicated.
        */
        const squares = this.state.squares.slice();

        //Already a winner
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            squares: squares,
            xIsNext: !this.state.xIsNext
        });
    }

    render() {
        const winner = calculateWinner(this.state.squares);
        let status;

        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }

        return (
            <div>
                <div className="status">{status}</div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

export default Board; 