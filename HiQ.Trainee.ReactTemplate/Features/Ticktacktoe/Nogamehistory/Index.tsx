﻿import * as React from "react";
import Board from "./Board";
import { css } from "../gameStyling"

interface Props {

}


class NoGameHistory extends React.Component<Props, void> {

   
    render() {
        return (
            <div >
                <style>
                    {css}
                </style>
                <div className="game">
                    <div className="game-board">
                        <Board />
                    </div>
                    <div className="game-info">
                        <div>{/* status */}</div>
                        <ol>{/* TODO */}</ol>
                    </div>
                </div>
            </div>
        );
    }
}

export default NoGameHistory;




