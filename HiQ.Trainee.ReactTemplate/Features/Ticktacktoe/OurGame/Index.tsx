﻿import * as React from "react";
import { css } from "../gameStyling"
import Board from "./Board";
import { calculateWinner } from '../rules';

interface Props {
}

interface State {
    history: history[],
    xIsNext: boolean,
    stepNumber: number
}


type history = {
    squares: Array<string>
}


class OurGame extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);

        this.state = {
            history: [{
                squares: Array(9).fill(null)
            }],
            xIsNext: true,
            stepNumber: 0
        }

    }


    handleClick(i: number) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        if (calculateWinner(squares) || squares[i]) {
            return;
        }

        squares[i] = this.state.xIsNext ? 'X' : 'O';

        this.setState({
            history: history.concat([{
                squares: squares
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext
        });
    }

    jumpTo(step: number) {
        this.setState({
            stepNumber: step, 
            xIsNext: (step % 2) ? false: true 
        })
    }
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);

        const moves = history.map((step: history, move: number) => {
            const desc = move ?
                'Move #' + move :
                'Game start';
            return (
                <li key={move}>
                    <a href="#" onClick={(e) => {e.preventDefault(); this.jumpTo(move)}}>{desc}</a>
                </li>
            )
        })

        let status: string;
        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next playaah: ' + (this.state.xIsNext ? 'X' : 'O');
        }
        return (
            <div >
                <style>
                    {css}
                </style>
                <div className="game">
                    <div className="game-board">
                        <Board
                            handleClick={(i: number) => this.handleClick(i)}
                            squares={current.squares}
                        />
                    </div>
                    <div className="game-info">
                        <div> {status} </div>
                        <ol>{moves}</ol>
                    </div>
                </div>
            </div>
        );
    }
}

export default OurGame;




