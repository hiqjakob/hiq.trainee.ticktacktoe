﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from "react";
import * as ReactDOM from "react-dom";
import * as ReactRouter from "react-router";
import { Switch, Route, Redirect } from "react-router-dom";
import { Col, Nav, NavItem, Badge } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
// import { Test } from "./TestComponent/test";
import { Menu } from "./Menu";
import NoGameHistory from "./Ticktacktoe/NoGamehistory";
import GameHistory from "./Ticktacktoe/Gamehistory"; 
import OurGame from "./Ticktacktoe/Ourgame";

interface Props {

}

export class App extends React.Component<Props, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            products: []
        }
    }

    componentDidMount() {
        fetch('/api/inventory/test').then((response) => {
            return response.text();
        }).then((inventory) => {
            this.setState({ products: JSON.parse(inventory) });
        });
    }


    render() {

        return (
            <main>
                <Menu />

                <Col lg={12} >
                    <div>
                        <Switch>
                            <Route exact path="/" component={() => (<Redirect to="/ticktacktoe/nogamehistory" />)} />
                            <Route path='/ticktacktoe/nogamehistory' component={() => (<NoGameHistory />)} />
                            <Route path='/ticktacktoe/gamehistory' component={() => (<GameHistory />)} />
                            <Route path='/ticktacktoe/ourgame' component={() => (<OurGame />)} />
                        </Switch>
                    </div>
                </Col>
            </main>
        );
    }
}