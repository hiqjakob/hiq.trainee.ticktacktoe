﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from "react";
import * as ReactDOM from "react-dom";

interface IInventoryRowProps {
    product: HiQ.InventoryViewModel,
}

class InventoryRow extends React.Component<IInventoryRowProps, any> {
    render() {
        return (
            <tr>
                <td>{this.props.product.Name}</td>
                <td>{this.props.product.ArticleNumber}</td>
            </tr>
        );
    }
}

interface IInventoryTableProps {
    products?: HiQ.InventoryViewModel[]
}

export class InventoryTable extends React.Component<IInventoryTableProps, any> {
    constructor(props: any) {
        super(props);
        this.state = {
        }
    }

    render() {
        var rows: any = [];
        if (this.props.products)
        {
            this.props.products.forEach(function (product: HiQ.InventoryViewModel, index:number ) {
                rows.push(<InventoryRow product={product} key={index} />);
            });
        }

        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Namn</th>
                        <th>Artikelnummer</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
}