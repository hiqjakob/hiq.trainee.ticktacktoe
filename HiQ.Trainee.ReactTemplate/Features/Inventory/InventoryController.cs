﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using HiQ.Trainee.ReactTemplate.Features.Inventory.ViewModels;

namespace HiQ.Trainee.ReactTemplate.Controllers
{
    [Authorize]
    [RoutePrefix("api/inventory")]
    public class InventoryController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("test")]
        public IHttpActionResult Test()
        {
            var list = new List<InventoryViewModel>();

            var item = new InventoryViewModel();

            item.ArticleNumber = 123;
            item.Name = "Test item";

            list.Add(item);

            return Ok(list);
        }
    }
}
