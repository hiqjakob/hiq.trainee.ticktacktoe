﻿// A '.tsx' file enables JSX support in the TypeScript compiler, 
// for more information see the following page on the TypeScript wiki:
// https://github.com/Microsoft/TypeScript/wiki/JSX
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Col } from "react-bootstrap";
import ImageGallery from 'react-image-gallery';
import { InventoryTable } from './InventoryTable';
import "react-image-gallery/styles/css/image-gallery.css";

interface IInventory {
    products?: HiQ.InventoryViewModel[]
}

export class Inventory extends React.Component<IInventory, IInventory> {
    constructor(props: any) {
        super(props);
        this.state = {
            products: this.props.products
        }
    }

    render() {
        return (
            <div >
                    <InventoryTable products={this.state.products} />
            </div >
        );
    }
}