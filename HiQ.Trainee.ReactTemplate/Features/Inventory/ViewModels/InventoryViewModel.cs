﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TypeLite;

namespace HiQ.Trainee.ReactTemplate.Features.Inventory.ViewModels
{
    [TsClass(Module = "HiQ")]
    public class InventoryViewModel
    {
        public string Name { get; set; }

        public int ArticleNumber { get; set; }
    }
}